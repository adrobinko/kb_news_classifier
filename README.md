# Резуальтаты

[файл с новостями по релевантности](data/interm/news_large_ranked.csv.gzip)

[тест](test.ipynb)

[код](lib)

<sub>*класс TextRanker использует токенизированные данные, которые сохранены в [data/tokenized](data/tokenized), а также использует ранее обученные модели для поиска факторов (lgbm_clf) и классификации предложений (tfidf_clf)<sub>


# Описание алгоритма

- Токенизируем все тексты с помощью UDPipe. Распараллелил на 32 потока.

    [tokenize notebook](notebooks/1.0-tokenize.ipynb)

- Строим словари факторов:
    - Возьмем 3 фактора (субъект "руководитель, директор", действие "бежать, скрываться", объект "рубеж, страна").
    - С помощью word2vec для каждого фактора найдем ближайшие токены. 
    
    [factors notebook](notebooks/2.0-w2v-factors.ipynb)
    
- Строим фичи:
    - Разбиваем статьи на предложения и ищем пересечения с факторами. 
    - Считаем кол-во совместных пересечений(пересечений в одном предложении).
    
- Обучаем классификатор:
    - Добавляем негативных примеров из теста(принимаем что все семплы не релевантны, target=0).
    - Обучаем классификатор на фичах.
    
    [factors classifier notebook](notebooks/3.0-factors-classifier.ipynb)

- Классификатор по предложениям.

    Мы научились находить факторы в предложениях. Используем это, чтобы разметить новый трейн сет.
    Будем считать, что предложения из размеченного датасета с совместным пересечениями 2 и 3 фактора("сбегать", "страна") релевантны, остальным предложениям присвоим target=0.
    На tf-idf этих предложений построим LgisticRegression.

    [sentence classifier notebook](notebooks/4.0-sentence-classifier.ipynb)

- Визуализация и финальное ранжирование
    
    Визуализатор запускается на чистом тексте.
    Делит на предложения, токенизирует, ищет факторы. Затем подсвечивает прерложение по кол-ву пересечений.

    [image](docs/visualizer-example.png)    

    [visualization notebook](notebooks/5.0-visualization.ipynb)


## Что делать дальше:

- Отбор факторов (валидируясь на размеченном датасете найти недостающие токены (повысим recall), валидируясь на результатах, убрать лишнее (повысим precision). Добавить NER(имена могут заменять первый фактор)
  Интерпретировать токены через LogisticRegression coef_.
- При хорошем recall на трейне, разметить топ 100 выдачи. Выбрать метрику ранжирования, например map@k, и в дальнейшем оценивать качество модели по ней. Настроить гиперпараметры моделей.
- Генерация фичей: использовать окна в 2-3 предложения для определения совместных пересечений. А также скользящие окна в n токенов.
- Активное обучение. Визуально проверить результаты, разметить, расширить трейн.
- Тематическое моделирование (определять тематику всей статьи, отранжировать ближайших к размеченному датасету)
- Строить графы синтаксических зависимостей(root, aux, det..), где узлом будет векторное представление токена.
