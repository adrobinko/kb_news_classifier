import ast
import pandas as pd
import pickle
import numpy as np

from sklearn.externals import joblib

from lib.features import find_factors, get_features


def predict_batch(path, factors, clf):
    print(f'Running on {path}..')
    df = pd.read_csv(path, compression='gzip')
    df.columns = ['index', 'tokens']
    df['tokens'] = df['tokens'].map(ast.literal_eval)
    df['f_intersections'] = df.tokens.map(
        lambda x: find_factors(x, factors, return_count=True))
    df['preds'] = clf.predict_proba(get_features(df, factors))[:, 1]
    return df


def predict_by_sentence(t, clf):
    sentence_intersections = []
    split_indeces = [i for i, x in enumerate(t) if x == "._PUNCT"]
    slice_from = 0
    for i, idx in enumerate(split_indeces):
        sentence = t[slice_from:idx]
        sentence.append('в_ADP')
        sentence_intersections.append(clf.predict_proba([sentence])[:, 1])
        slice_from = idx + 1

    return sentence_intersections


class TextRanker:
    def __init__(self, batch_paths: list):
        self.batch_paths = batch_paths
        self.factors = ()
        self.factor_clf = None
        self.tfidf_clf = None

    def load(self):
        with open('data/interm/factors.pickle', 'rb') as f:
            self.factors = pickle.load(f)
        self.factor_clf = joblib.load('models/lgbm_clf.joblib')
        self.tfidf_clf = joblib.load('models/tfidf_clf.joblib')

    def predict(self):
        factors = self.predict_factors()
        factors = self.add_heuristics(factors)
        factors_rank = factors[['index', 'rank']]
        top_rank = self.predict_sentences(factors[factors['preds'] > 0.2])
        total_rank = self.total_ranking(factors_rank, top_rank)
        return total_rank

    def predict_factors(self) -> pd.DataFrame:
        df = pd.concat([predict_batch(path, self.factors, self.factor_clf) for path in self.batch_paths])
        return df

    def add_heuristics(self, df):
        f1, f2, f3 = self.factors
        df['fac1'] = df.tokens.map(lambda x: f1.intersection(x))
        df['fac2'] = df.tokens.map(lambda x: f2.intersection(x))
        df['fac3'] = df.tokens.map(lambda x: f3.intersection(x))

        df['preds'] = df.assign(penalty=(
                (df['fac2'] == {'уходить_VERB'}) |
                (df['fac2'] == {'уводить_VERB'}))).apply(
            lambda row: row['preds'] - 0.6 if row['penalty'] else row['preds'],
            axis=1)

        df['preds'] = df.assign(penalty=(
            df.f_intersections.map(lambda x: 3 not in x))).apply(
            lambda row: row['preds'] - 0.1 if row['penalty'] else row['preds'],
            axis=1)

        df.sort_values('preds', ascending=False, inplace=True)
        df['rank'] = df.reset_index().index
        return df

    def predict_sentences(self, df):
        df['sent_preds'] = df['tokens'].map(
            lambda x: predict_by_sentence(x, self.tfidf_clf))
        df['s_preds_max'] = df.sent_preds.map(max)
        df.sort_values('s_preds_max', ascending=False, inplace=True)
        df['s_preds_max'] = df['s_preds_max'].map(lambda x: x[0])
        df['blended_preds'] = df['preds'] + 2 * df['s_preds_max']
        df.sort_values('blended_preds', ascending=False, inplace=True)
        return df[['index', 'blended_preds']]

    @staticmethod
    def total_ranking(factors_rank, top_rank) -> np.array:
        total_ranking = pd.merge(factors_rank,
                                 top_rank[['index', 'blended_preds']],
                                 how='left', on='index')
        total_ranking.sort_values(['blended_preds', 'rank'],
                                  ascending=[False, True], inplace=True)
        return total_ranking['index']




