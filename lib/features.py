import pandas as pd

from collections import Counter


def flattener(arr: list) -> list:
    return [item for sublist in arr for item in sublist]


def find_factors(text: list,
                 factors: tuple,
                 return_count: bool = False) -> list:
    """Find mutual factor intersections for each sentence."""
    sentence_intersections = []
    split_indeces = [i for i, token in enumerate(text) if token == '._PUNCT']
    slice_from = 0

    for i, index in enumerate(split_indeces):
        sentence = text[slice_from:index]
        intersections = list()

        for el in sentence:
            for j, factor in enumerate(factors):
                if el in factor:
                    intersections.append(j)
        slice_from = index + 1
        if return_count:
            sentence_intersections.append(len(set(intersections)))
        else:
            sentence_intersections.append(intersections)
    return sentence_intersections


def get_features(df: pd.DataFrame, factors: tuple) -> pd.DataFrame:
    features = pd.DataFrame()
    temp_df = df['f_intersections'].map(lambda x: Counter(x))

    features['f0'] = temp_df.map(lambda x: x[0])
    features['f1'] = temp_df.map(lambda x: x[1])
    features['f2'] = temp_df.map(lambda x: x[2])
    features['f3'] = temp_df.map(lambda x: x[3])
    features['f_summ'] = features[['f1', 'f2', 'f3']].applymap(
        lambda x: x > 0).T.sum()

    temp_df = df.tokens.map(
        lambda x: find_factors(x, factors)).map(flattener).map(Counter)
    for i in range(len(factors)):
        features[f'f{i+1}_count'] = temp_df.map(lambda x: x[i])
    return features
